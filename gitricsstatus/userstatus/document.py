import datetime
import os
import re

import markdown

from bs4 import BeautifulSoup

from gitrics.group.group import gitricsGroup

from gitricsstatus import configuration
from gitricsstatus.dataloader import DataLoader
from gitricsstatus.partials import ACTIVITY, DATA_INPUT, MEETINGS, ROADMAP, TASKS

from glapi import configuration as configuration_glapi

from mkdpdf import configuration as configuration_mkdpdf
from mkdpdf.report.report import Report

class UserStatus(Report):
    """
    UserStatus is an abstraction to represent a document.
    """

    def __init__(self, directory_path_output: str = configuration_mkdpdf.DIRECTORY_PATH_OUTPUT, directory_path_templates: str = None, email_host: str = None, title: str = None):
        """
        Args:
            directory_path_output (string): path of output directory
            directory_path_templates (string): path of template directory
            email_host (string): email domain from which to send
            title (string): title of document
        """

        # initialize inheritance
        super(UserStatus, self).__init__(
            directory_path_output=directory_path_output,
            directory_path_templates=directory_path_templates,
            email_host=email_host,
            title=title
        )

    def create(self, data: list, gg: gitricsGroup,  date_publish_future: str, date_publish_past: str, date_meeting: str, date_publish: str, pattern_missing: str = None, pattern_restricted: str = None) -> str:
        """
        Create status document.

        Args:
            data (list): classes of GitlabEpic or GitlabIssue
            date_meeting (string): iso 8601 date value
            date_publish (string): iso 8601 date value
            date_publish_future (string): iso 8601 date value
            date_publish_past (string): iso 8601 date value
            gg (gitricsGroup): group metrics class
            pattern_missing (string): RegEx pattern representing a missing annotation
            pattern_restricted (string): RegEx pattern representing a restricted annotation

        Returns:
            A string representing a status document.
        """

        # convert dates to datetime
        DATE_PUBLISH_FUTURE_DT = datetime.datetime.strptime(date_publish_future, configuration_glapi.DATE_ISO_8601)
        DATE_PUBLISH_PAST_DT = datetime.datetime.strptime(date_publish_past, configuration_glapi.DATE_ISO_8601)

        # initialize items and graph
        gg.epics.items, gg.epics.graph = gg.initialize_epics(epics=[d for d in data if d.gitlab_type == "epic"])
        gg.issues.items, gg.issues.graph = gg.initialize_issues(issues=[d for d in data if d.gitlab_type == "issue"])

        # initialize parsed status data
        dl = DataLoader(group=gg.group, epics=gg.epics, issues=gg.issues)

        # condition data
        dl.condition(pattern_missing, pattern_restricted)

        # organize data
        data = dl.categorize(DATE_PUBLISH_FUTURE_DT, DATE_PUBLISH_PAST_DT)

        # format visual display
        doc = self.publish(data, date_meeting, date_publish, DATE_PUBLISH_FUTURE_DT, DATE_PUBLISH_PAST_DT)

        # clean up extra space
        content = re.sub(r"\r+", str(), doc)

        return content

    def publish(self, data, date_meeting: str, date_publish: str, date_publish_future_dt: datetime.datetime, date_publish_past_dt: datetime.datetime) -> str:
        """
        Publish document merging data in specified format.

        Args:
            data (Data): class of attribute for document render
            date_meeting (string): iso 8601 date value
            date_publish (string): iso 8601 date value
            date_publish_future_dt (datetime): future publish date
            date_publish_past_dt (datetime): past publish date

        Returns:
            A string representing document in specified format.
        """

        # prune meetings for those with dates
        mtgs = sorted([d for d in data.meetings if getattr(d, d.gitlab_type)["due_date"] and datetime.datetime.strptime(getattr(d, d.gitlab_type)["due_date"], configuration_glapi.DATE_ISO_8601) >= date_publish_past_dt], key=lambda d: getattr(d, d.gitlab_type)["due_date"]) if data.meetings else None

        # generate meetings list
        with open(os.path.join(self.directory_path_templates, "meetings.md")) as f: partial = f.read()
        MTGS = self.transpile(template=partial, section={
            "DATE_PUBLISH_FUTURE": date_publish_future_dt.strftime("%d %B %Y"),
            "SUBTEMPLATE_MEETINGS": MEETINGS(mtgs) if mtgs else str()
        })

        # generate roadmap
        with open(os.path.join(self.directory_path_templates, "roadmap.md")) as f: partial = f.read()
        RDM = self.transpile(template=partial, section={
            "DATE_PUBLISH_FUTURE": date_publish_future_dt.strftime("%d %B %Y"),
            "SUBTEMPLATE_ONGOING": ROADMAP("Ongoing", data.ongoing) if data.ongoing else str(),
            "SUBTEMPLATE_PASTDUE": ROADMAP("Past Due", data.pastdue) if data.pastdue else str(),
            "SUBTEMPLATE_UPCOMING": ROADMAP("Upcoming", data.upcoming) if data.upcoming else str()
        })

        # generate schedule
        with open(os.path.join(self.directory_path_templates, "schedule.md")) as f: partial = f.read()
        SCHEDULE = self.transpile(template=partial, section={
            "DATE_FUTURE": date_publish_future_dt.strftime("%d %B %Y"),
            "DATE_PAST": date_publish_past_dt.strftime("%d %B %Y"),
            "SUBTEMPLATE_MEETINGS": MTGS if mtgs else str(),
            "SUBTEMPLATE_ROADMAP": RDM if data.ongoing or data.pastdue or data.upcoming else str()
        })

        # generate markdown
        return self.generate(
            header=str(),
            main={
                "DATE_MEETING": (datetime.datetime.strptime(date_meeting, configuration_glapi.DATE_ISO_8601)).strftime("%d %B %Y"),
                "DATE_PUBLISH": (datetime.datetime.strptime(date_publish, configuration_glapi.DATE_ISO_8601)).strftime("%d %B %Y"),
                "STATUS_TITLE": self.title,
                "SUBTEMPLATE_ACTIVITY": ACTIVITY(data.completed, data.keyevents, data.risks, date_publish_past_dt) if len(data.completed) > 0 or len(data.keyevents) > 0 or len(data.risks) > 0 else str(),
                "SUBTEMPLATE_DATAINPUT": DATA_INPUT(data.missing, data.restricted) if len(data.missing) > 0 or len(data.restricted) > 0 else str(),
                "SUBTEMPLATE_SCHEDULE": SCHEDULE if mtgs or data.ongoing or data.pastdue or data.upcoming else str(),
                "SUBTEMPLATE_TASKS": TASKS(data.blocking, data.blocked) if len(data.blocking) > 0 or len(data.blocked) > 0 else str(),
                "URL_MEETING": "https://blah.com",
                "URL_ROADMAP": "https://blah.roadmap"
            },
            footer=str()
        )

    def format_email(self, content: str, accent: str = configuration.EMAIL_ACCENT_COLOR, accent_light: str = configuration.EMAIL_ACCENT_COLOR_LIGHT, table_fill: str = configuration.EMAIL_TABLE_FILL_COLOR, line_height: str = configuration.EMAIL_LINE_HEIGHT) -> str:
        """
        Format Markdown content for email render.

        Args:
            accent (string): link accent color
            content (string): Markdown partial representing status document
            line_height (float): typographic line height (leading)
            table_fill (string): table fill color

        Returns:
            A string of HTML representing an email-specific render of a status document.
        """

        # convert markdown to html
        html = markdown.markdown(content, extensions=["markdown.extensions.tables"])

        # wrap with div for styling
        html = '<div style="font-style: sans-serif">%s</div>' % html

        # clean up empty space
        html = html.replace("\n", "")

        # initialize parser
        soup = BeautifulSoup(html, "html.parser")

        # HEADLINES
        h1 = soup.find_all("h1")
        for tag in h1:
            tag["style"] = "margin: 0; line-height: %s" % line_height

        h2 = soup.find_all("h2")
        for tag in h2:
            tag["style"] = "margin: 0; margin-top: 2em; border-top: 0.2em; border-top-style: solid; padding-top: 0.5em;"

        h3 = soup.find_all("h3")
        for tag in h3:
            tag["style"] = "margin: 0; margin-top: 1em; margin-bottom: 0.25em; line-height: %s;" % line_height

        # TEXT
        para = soup.find_all("p")
        for i, tag in enumerate(para):
            if "Completed" in tag.text and "Ongoing" in tag.text and "Past Due" in tag.text and "Upcoming" in tag.text:
                tag["style"] = "margin: 0; max-width: 500px; line-height: %s; font-size: 0.8em;" % line_height
            else:
                tag["style"] = "margin: 0; max-width: 500px; line-height: %s;" % line_height

        a = soup.find_all("a")
        for tag in a:
            tag["style"] = "color: %s;" % accent

        em = soup.find_all("em")
        for tag in em:
            tag["style"] = "color: grey; margin-bottom: 0.75em; display: block;"

        ul = soup.find_all("ul")
        for tag in ul:
            tag["style"] = "max-width: 500px; line-height: %s;" % line_height

        # TABLES
        table = soup.find_all("table")
        for tag in table:
            tag["style"] = "border-collapse: collapse;"

        th = soup.find_all("th")
        for i, tag in enumerate(th):
            if i == 0:
                tag["style"] = "padding: 5px; padding-left: 10px; padding-right: 10px; min-width: 200px"
            else:
                tag["style"] = "padding: 5px; padding-left: 10px; padding-right: 10px;"

        tr = soup.find_all("tr")
        for i, tag in enumerate(tr):
            if i == 0:
                tag["style"] = "border-bottom: 0.2em; border-bottom-style: solid;"
            if i % 2 != 0:
                tag["style"] = "background-color: %s;" % table_fill

        td = soup.find_all("td")
        for tag in td:
            tag["style"] = "padding: 10px; line-height: %s; vertical-align: top;" % line_height
            b = tag.find_all("strong")
            for t in b:
                t["style"] = "font-size: 1.35em;"

        # convert back into string from soup
        return str(soup)
