import datetime

from gitricsstatus import configuration

from glapi import configuration as configuration_glapi

from mkdpdf import configuration as configuration_mkdpdf

def ACTIVITY(completed: list = None, keyevents: list = None, risks: list = None, date_publish_past_dt: datetime.datetime = None) -> str:
    """
    Generate activity section partial.

    Args:
        completed (list): classes of a gitrics<Object>
        date_publish_past_dt (datetime): past publish date
        keyevents (list): classes of a gitrics<Object>
        risks (list): classes of a gitrics<Object>

    Returns:
        A string of markdown representing the activity section.
    """

    # format dates
    date = date_publish_past_dt.strftime("%d %B %Y") if date_publish_past_dt else None

    # sort desc by date
    # completed
    c = sorted(completed, key=lambda d: (datetime.datetime.strptime(getattr(d, d.gitlab_type)["closed_at"], configuration_glapi.DATE_ISO_8601_TIME)).isoformat(), reverse=True) if completed else None

    # keyevents
    ke = sorted(keyevents, key=lambda d: (datetime.datetime.strptime(getattr(d, d.gitlab_type)["due_date"], configuration_glapi.DATE_ISO_8601)).isoformat() if getattr(d, d.gitlab_type)["due_date"] else (datetime.datetime.strptime(getattr(d, d.gitlab_type)["closed_at"], configuration_glapi.DATE_ISO_8601_TIME)).isoformat(), reverse=True) if keyevents else None

    # risks
    r = sorted(risks, key=lambda d: (datetime.datetime.strptime(getattr(d, d.gitlab_type)["updated_at"], configuration_glapi.DATE_ISO_8601_TIME)).isoformat(), reverse=True) if risks else None

    return configuration_mkdpdf.GITFLAVOR_RETURN.join([
        "## Activity Report Updates",
        COMPLETED(c, date),
        KEY_EVENTS(ke, date),
        RISKS(r, date)
    ])

def COMPLETED(completed: list = None, date: datetime.datetime = None) -> str:
    """
    Generate completed activity partial.

    Args:
        completed (list): classes of a gitrics<Object>
        date (datetime): past publish date

    Returns:
        A string of markdown representing the completed activity section.
    """

    return configuration_mkdpdf.GITFLAVOR_RETURN.join([
        "### Completed",
        "_The following items were marked complete after the last status (%s)._" % date,
        configuration_mkdpdf.GITFLAVOR_RETURN.join([
            "%s%s" % (
                "%s %s" % (
                    LINKED_TITLE(d),
                    "_(completed %s)_" % (
                        (datetime.datetime.strptime(getattr(d, d.gitlab_type)["due_date"], configuration_glapi.DATE_ISO_8601)).strftime("%A") if getattr(d, d.gitlab_type)["due_date"] else (datetime.datetime.strptime(getattr(d, d.gitlab_type)["closed_at"], configuration_glapi.DATE_ISO_8601_TIME)).strftime("%A")
                    )
                ),
                "%s%s" % (
                    configuration_mkdpdf.GITFLAVOR_RETURN,
                    getattr(d, d.gitlab_type)["description"]
                ) if getattr(d, d.gitlab_type)["description"] and d.has_restricted_annotation is False and d.is_missing_annotation is False else str()
            )
            for d in completed
        ])
    ]) if completed and date else str()

def DATA_INPUT(missing: list = None, restricted: list = None) -> str:
    """
    Generate data input section partial.

    Args:
        missing (list): classes of a gitrics<Object>
        restricted (list): classes of a gitrics<Object>

    Returns:
        A string of markdown representing the data input section.
    """

    return configuration_mkdpdf.GITFLAVOR_RETURN.join([
        "## Data Input",
        configuration_mkdpdf.GITFLAVOR_RETURN.join([
            "### Missing Annotation",
            "_The following items are missing annotation in the title or description and will be masked until properly annotated._",
            configuration_mkdpdf.GITFLAVOR_RETURN.join([LINKED_TITLE(d) for d in missing])
        ]) if missing else str(),
        configuration_mkdpdf.GITFLAVOR_RETURN.join([
            "### Restricted Annotation",
            "_The following items contain restricted annotation in the title or description and will be masked until properly annotated for autoreporting._",
            configuration_mkdpdf.GITFLAVOR_RETURN.join([LINKED_TITLE(d) for d in restricted])
        ]) if restricted else str()
    ])

def FOCI(foci: list, date_publish_past_dt: datetime.datetime) -> str:
    """
    Generate foci section partial.

    Args:
        date_publish_past_dt (datetime): past publish date
        labels (foci): strings of GitLab label values

    Returns:
        A string of a Markdown partial representing a foci section.
    """

    return configuration_mkdpdf.GITFLAVOR_RETURN.join([
        configuration_mkdpdf.GITFLAVOR_RETURN.join([
            "### %s" % k,
            configuration_mkdpdf.GITFLAVOR_RETURN.join([
                "%s _%s_ %s" % (
                    LINKED_TITLE(d),
                    ",".join([x.user["name"] for x in d.ownership]) if d.ownership else str(),
                    "(completed this week)" if d.status == "complete" and datetime.datetime.strptime(getattr(d, d.gitlab_type)["closed_at"], configuration_glapi.DATE_ISO_8601_TIME) >= date_publish_past_dt else str()
                )
                for d in foci[k]
            ]) if len(foci[k]) > 0 else "No Data."
        ])
        for k in foci
    ])

def KEY_EVENTS(keyevents: list = None, date: datetime.datetime = None) -> str:
    """
    Generate key event section partial.

    Args:
        date (datetime): past publish date
        keyevents (list): classes of a gitrics<Object>

    Returns:
        A string of markdown representing the activity section.
    """

    return configuration_mkdpdf.GITFLAVOR_RETURN.join([
        "### Key Events",
        "_The following items were marked as accomplishments after the last status (%s)._" % date,
        configuration_mkdpdf.GITFLAVOR_RETURN.join([
            "%s%s" % (
                "%s %s" % (
                    LINKED_TITLE(d),
                    "_(%s)_" % (
                        (datetime.datetime.strptime(getattr(d, d.gitlab_type)["due_date"], configuration_glapi.DATE_ISO_8601)).strftime("%d %B %Y") if getattr(d, d.gitlab_type)["due_date"] else (datetime.datetime.strptime(getattr(d, d.gitlab_type)["closed_at"], configuration_glapi.DATE_ISO_8601_TIME)).strftime("%A")
                    )
                ),
                "%s%s" % (
                    configuration_mkdpdf.GITFLAVOR_RETURN,
                    getattr(d, d.gitlab_type)["description"]
                ) if getattr(d, d.gitlab_type)["description"] and d.has_restricted_annotation is False and d.is_missing_annotation is False else str()
            )
            for d in keyevents
        ])
    ]) if keyevents and date else str()

def LINKED_TITLE(item) -> str:
    """
    Generate linked title.

    Args:
        item (enum): gitricsEpic or gitricsIssue

    Returns:
        A string of a Markdown partial representing a single GitLab object title/web url.
    """

    return "[%s](%s)" % (
        getattr(item, item.gitlab_type)["title"] if hasattr(item, "has_restricted_annotation") and hasattr(item, "is_missing_annotation") and item.has_restricted_annotation is False and item.is_missing_annotation is False else
        "%s %s" % (
            item.gitlab_type.title(),
            item.id
        ),
        getattr(item, item.gitlab_type)["web_url"]
    )

def METRICS(top_blockers: list = None, top_at_risk: list = None, top_on_track: list = None) -> str:
    """
    Generate metrics section partial.

    Args:
        top_at_risk (list): classes of a gitrics<Object>
        top_blockers (list): classes of a gitrics<Object>
        top_on_track (list): classes of a gitrics<Object>

    Returns:
        A string of markdown representing the metrics section.
    """

    return configuration_mkdpdf.GITFLAVOR_RETURN.join([
        "Top blockers: %s" % ", ".join([LINKED_TITLE(d) for d in top_blockers]) if top_blockers else str(),
        "Most at risk: %s" % ", ".join([LINKED_TITLE(d) for d in top_at_risk]) if top_at_risk else str(),
        "Most on track: %s" % ", ".join([LINKED_TITLE(d) for d in top_on_track]) if top_on_track else str()
    ])

def MEETINGS(meetings: list) -> str:
    """
    Generate meetings partial section.

    Args:
        meetings (list): classes of gitrics<Object>

    Returns:
        A string of markdown representing the meetings section.
    """

    return configuration_mkdpdf.GITFLAVOR_BREAK_RETURN.join([
        "| Title | Date |",
        "| :-- | :-- |",
        configuration_mkdpdf.GITFLAVOR_BREAK_RETURN.join([
            "| %s | %s |" % (
                LINKED_TITLE(d),
                (datetime.datetime.strptime(getattr(d, d.gitlab_type)["due_date"], configuration_glapi.DATE_ISO_8601)).strftime("%A, %d %B %Y")
            )
            for d in meetings
        ])
    ])

def NESTED_TABLE(rows: list) -> str:
    """
    Generate HTML table where level-1 nodes are rows and nested subgraphs are in the same row under each level-1.

    Args:
        rows (list): classes of gitricsEpic or gitricsIssue

    Returns:
        An Markdown string partial representing a nested graph in tabular format.
    """

    htmlspace = "&emsp;&emsp;&emsp;&emsp;&emsp;"

    return "| Title | Status | Progress |%s| :-- | :-- | --: |%s%s" % (
        configuration_mkdpdf.GITFLAVOR_BREAK_RETURN,
        configuration_mkdpdf.GITFLAVOR_BREAK_RETURN,
        "".join([
            "| %s | %s | %s |%s" % (
                "%s<br>%s" % (
                    LINKED_TITLE(d),
                    "<br>".join([
                        "%s%s&emsp;%s" % (
                            "".join([htmlspace for y in range(x.graph_depth - 1)]),
                            configuration.ICON_UPCOMING if x.status == "upcoming" else (configuration.ICON_COMPLETE if x.status == "complete" else (configuration.ICON_PASTDUE if x.status == "past due" else configuration.ICON_ONGOING)),
                            LINKED_TITLE(x)
                        )
                        for x in d.descendants
                    ])
                ),
                d.status,
                "{}%".format(d.percent_complete),
                configuration_mkdpdf.GITFLAVOR_BREAK_RETURN
            ) for d in rows
        ])
    )

def ROADMAP(title: str, table_rows: list) -> str:
    """
    Generate roadmap section partial.

    Args:
        table_rows (list): classes of gitricsEpic or gitricsIssue
        title (string): title of table

    Returns:
        A string of markdown representing the roadmap section.
    """

    space = "&ensp;"

    return configuration_mkdpdf.GITFLAVOR_RETURN.join([
        "#### %s" % title,
        "&emsp;".join([
            "%s%sCompleted" % (configuration.ICON_COMPLETE, space),
            "%s%sOngoing" % (configuration.ICON_ONGOING, space),
            "%s%sPast Due" % (configuration.ICON_PASTDUE, space),
            "%s%sUpcoming" % (configuration.ICON_UPCOMING, space)
        ]),
        NESTED_TABLE(table_rows)
    ])

def RISKS(risks: list = None, date: datetime.datetime = None) -> str:
    """
    Generate activity section partial.

    Args:
        date (datetime): past publish date
        risks (list): classes of a gitrics<Object>

    Returns:
        A string of markdown representing the risks section.
    """

    return configuration_mkdpdf.GITFLAVOR_RETURN.join([
        "### Risks",
        "_The following items were marked `at risk` after the last status (%s)._" % date,
        configuration_mkdpdf.GITFLAVOR_RETURN.join([
            "%s %s%s" % (
                LINKED_TITLE(d),
                "_(marked %s)_" % (datetime.datetime.strptime(getattr(d, d.gitlab_type)["updated_at"], configuration_glapi.DATE_ISO_8601_TIME)).strftime("%A"),
                "%s%s" % (
                    configuration_mkdpdf.GITFLAVOR_RETURN,
                    getattr(d, d.gitlab_type)["description"]
                ) if getattr(d, d.gitlab_type)["description"] and d.has_restricted_annotation is False and d.is_missing_annotation is False else str()
            )
            for d in risks
        ])
    ]) if risks and date else str()

def TASKS(blocking: list = None, blocked: list = None) -> str:
    """
    Generate tasks section partial.

    Args:
        blocked (list): classes of a gitrics<Object>
        blocking (list): classes of a gitrics<Object>

    Returns:
        A string of markdown representing the tasks section.
    """

    return configuration_mkdpdf.GITFLAVOR_RETURN.join([
        "## Tasks",
        configuration_mkdpdf.GITFLAVOR_RETURN.join([
            "### You Are Blocking",
            "_The following items are blocking the progress of others._",
            configuration_mkdpdf.GITFLAVOR_RETURN.join([LINKED_TITLE(d) for d in blocking])
        ]) if blocking else str(),
        configuration_mkdpdf.GITFLAVOR_RETURN.join([
            "### Your Progress is Blocked",
            "_The following items are blocked by others._",
            configuration_mkdpdf.GITFLAVOR_RETURN.join([LINKED_TITLE(d) for d in blocked])
        ]) if blocked else str()
    ])
