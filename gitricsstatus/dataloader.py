import datetime
import re

import networkx as nx

from gitrics import configuration as configuration_gitrics
from gitrics.group.group import gitricsGroup, gitricsItemSet
from gitrics.utilities import ci_lower_bound

from gitricsstatus import configuration

from glapi import configuration as configuration_glapi

class Data:
    def __init__(self):
        pass

class DataLoader(gitricsGroup):
    """
    gitricsStatus is an opinionated data format and report generator for the gitrics ecosystem.
    """

    def __init__(self, group: dict = None, epics: gitricsItemSet = None, issues: gitricsItemSet = None, token: str = configuration_gitrics.GITLAB_TOKEN, version: str = configuration_gitrics.GITLAB_API_VERSION):
        """
        Args:
            group (dict): key/values representing a Gitlab Group
            epics (gitricsItemSet): classes of gitricsEpic
            issues (gitricsItemSet): classes of gitricsIssue
            token (string): GitLab personal access or deploy token
            version (string): GitLab API version as base url
        """

        # initialize inheritance
        super(DataLoader, self).__init__(
            group=group,
            epics=epics,
            id=id,
            issues=issues,
            token=token,
            version=version
        )

    def annotate(self, item, pattern_missing: str = configuration.ANNOTATION_MISSING, pattern_restricted: str = configuration.ANNOTATION_RESTRICTED):
        """
        Properly annotate title and description for report generation.

        Args:
            item (enum): gitricsEpic || GitlabIssue
            pattern_missing (string): RegEx pattern representing a missing annotation
            pattern_restricted (string): RegEx pattern representing a restricted annotation
        """

        # isolate item
        d = getattr(item, item.gitlab_type)

        # remove any extra whitespace in titles
        title = re.sub(r"\s+", " ", d["title"])
        title = d["title"].lstrip()

        # split description into list on more than 2 spaces/newlines
        # remove any empty spaces
        MARKDOWN_BULLET = "^(\s+)?(\*|\-)(\s+)?(\[(x|(\s+?))\])?"
        description_tokens = [
            re.sub(MARKDOWN_BULLET, "", a, flags=re.MULTILINE).strip()
            for b in
            [
                [y for y in re.split("\s{2,}", x)]
                for x in re.split("\n", d["description"])
                if x != str()
            ]
            for a in b if a != ""
        ] if d["description"] else None

        # combine segments
        segments = [title] + description_tokens if description_tokens else [title]

        # check state since title or description would make attribute true
        is_missing_annotation = any([True for x in segments if re.match(pattern_missing, x) is None]) if pattern_missing else False
        has_restricted_annotation = any([re.match(pattern_restricted, x, re.IGNORECASE) for x in segments]) if pattern_restricted else False

        # update item
        item.is_missing_annotation = is_missing_annotation
        item.has_restricted_annotation = has_restricted_annotation

    def categorize(self, date_publish_future_dt: datetime.datetime, date_publish_past_dt: datetime.datetime, foci: list = None) -> Data:
        """
        Prune datasets to categories.

        Args:
            date_publish_future_dt (datetime): future publish date
            date_publish_past_dt (datetime): past publish date
            foci (list): strings of GitLab label value

        Returns:
            A Data object with attributes for status document.
        """

        # initialize data object
        d = Data()

        # data input
        d.missing, d.restricted = self.extract_annotations()

        # tasks
        d.blocking, d.blocked = self.extract_tasks()

        # activity
        d.completed, d.keyevents, d.risks = self.extract_activity(date_publish_past_dt)

        # schedule
        d.meetings, d.pastdue, d.ongoing, d.upcoming = self.extract_schedule(date_publish_future_dt)

        # itemset-level metrics
        d.top_blockers, d.top_at_risk, d.top_on_track = self.generate_metrics()

        # foci
        d.foci = self.extract_foci(foci)

        return d

    def condition(self, pattern_missing: str = None, pattern_restricted: str = None):
        """
        Condition data for report generation.

        Args:
            pattern_missing (string): RegEx pattern representing a missing annotation
            pattern_restricted (string): RegEx pattern representing a restricted annotation
        """

        # loop through sets of objects
        for itemset in [self.epics, self.issues]:

            # loop through unique object lists
            for item in itemset.items:

                # annotate
                self.annotate(item, pattern_missing, pattern_restricted)

                # enrich
                item.graph_depth = nx.shortest_path_length(itemset.graph, 0)[item.id] if itemset.graph and item.id in list(sum(list(itemset.graph.edges()), ())) else 1
                item.is_accomplishment = "accomplishment" in getattr(item, item.gitlab_type)["labels"]
                item.status = self.determine_status(getattr(item, item.gitlab_type))

        # epic-specific enrichment
        for item in self.epics.items:

            # generate subgraph
            sg = self.epics.graph.subgraph([item.id] + list(nx.descendants(self.epics.graph, item.id)))

            # prune lists to the nodes in the subgraph
            sg_epics = [d for d in self.epics.items if d.id in list(sum(list(sg.edges()), ()))]

            # enrich
            item.count_at_risk = len([d for d in item.issues if "health_status" in d.issue and d.issue["health_status"] == "at_risk"]) if item.issues else 0
            item.count_on_track = len([d for d in item.issues if "health_status" in d.issue and d.issue["health_status"] == "on_track"]) if item.issues else 0
            item.percent_complete = item.calculate_completion(sg_epics)
            item.score_at_risk = ci_lower_bound(item.count_at_risk, len(item.issues)) if item.issues else 0
            item.score_on_track = ci_lower_bound(item.count_on_track, len(item.issues)) if item.issues else 0

        # issue-specific enrichment
        for item in self.issues.items:

            # type of link relationships
            for lt in ["blocks", "is_blocked_by"]:

                # get link types
                linktypes = any(list(set([d.issue["link_type"] for d in item.links if d.issue["link_type"] == lt]))) if item.links else False

                # update item attribute w/relationship
                setattr(item, lt, linktypes)

    def extract_activity(self, date_publish_past_dt: datetime.datetime) -> tuple:
        """
        Extract items relating to closed, keyevents, and risks.

        Args:
            date_publish_past_dt (datetime): past publish date

        Retruns:
            A tuple of (0, 1, 2) where 0 is a list of items that were closed since past date; 1 is a list of items that were closed since past date and labeled "accomplishment"; 2 is a list of items that are open with a health status of "at risk" marked as such since past date.
        """

        # items completed since past date
        completed = [
            d for d in self.epics.items + self.issues.items
            if d.status == "complete"
            and datetime.datetime.strptime(getattr(d, d.gitlab_type)["closed_at"], configuration_glapi.DATE_ISO_8601_TIME) >= date_publish_past_dt
            and "meeting" not in getattr(d, d.gitlab_type)["labels"]
            and "accomplishment" not in getattr(d, d.gitlab_type)["labels"]
        ]

        # items completed since past date and labeled "accomplishment"
        keyevents = [
            d for d in self.epics.items + self.issues.items
            if d.is_accomplishment and d.status == "complete"
            and ((getattr(d, d.gitlab_type)["due_date"]
            and datetime.datetime.strptime(getattr(d, d.gitlab_type)["due_date"], configuration_glapi.DATE_ISO_8601_TIME) >= date_publish_past_dt) or (getattr(d, d.gitlab_type)["closed_at"]
            and datetime.datetime.strptime(getattr(d, d.gitlab_type)["closed_at"], configuration_glapi.DATE_ISO_8601_TIME) >= date_publish_past_dt))
        ]

        # items open with a health status "at_risk" marked since past date
        risks = [
            d for d in self.issues.items
            if d.status != "complete"
            and "health_status" in d.issue
            and d.issue["health_status"] == "at_risk"
            and d.notes
            and len([
                x for x in d.notes
                if x["type"] is None
                and x["body"] == "changed health status to **at risk**"
                and datetime.datetime.strptime(x["updated_at"], configuration_glapi.DATE_ISO_8601_TIME) >= date_publish_past_dt
            ]) > 0
        ]

        return (completed, keyevents, risks)

    def extract_annotations(self) -> tuple:
        """
        Extract items relating to annotation data.

        Returns:
            A tuple of (0, 1) where 0 is a list of items with missing annotations; 1 is a list of items with restricted annotations.
        """

        # items with missing annotations
        missing = [
            d for d in self.epics.items + self.issues.items
            if d.status != "complete" and d.is_missing_annotation
        ]

        # items with restricted annotations
        restricted = [
            d for d in self.epics.items + self.issues.items
            if d.status != "complete" and d.has_restricted_annotation
        ]

        return (missing, restricted)

    def extract_foci(self, labels: list):
        """
        Extract items with a focus label.

        Args:
            labels (list): list of GitLab labels

        Returns:
            A dictionary where keys are foci labels and corresponding values are lists of items tagged with that label.
        """

        result = None

        if labels:

            result = {
                k: [d for d in self.epics.items + self.issues.items if k in getattr(d, d.gitlab_type)["labels"]]
                for k in labels
            }

        return result

    def extract_schedule(self, date_publish_future_dt: datetime.datetime) -> tuple:
        """
        Extract items relating to scheduling data.

        Args:
            date_publish_future_dt (datetime): future publish date

        Returns:
            A tuple of (0, 1) where 0 is a list of items labeled "meeting"; 1 is a list items determined to be past due; 2 is a list of items determined to be ongoing; 3 is a list of items determined to be upcoming.
        """

        # generate nested tabular data
        epics = self.tabulate_nest(self.epics)

        # prune top-level items to only those with start/end
        pruned = sorted([
            d for d in epics
            if (getattr(d, d.gitlab_type)["start_date"] is None and getattr(d, d.gitlab_type)["end_date"] is None)
            or (getattr(d, d.gitlab_type)["start_date"] and datetime.datetime.strptime(getattr(d, d.gitlab_type)["start_date"], configuration_glapi.DATE_ISO_8601) <= date_publish_future_dt)
        ], key=lambda k: (-k.percent_complete, k.epic["title"])) if epics else None

        # open items labeled with "meeting"
        meetings = [
            d for d in pruned
            if "meeting" in getattr(d, d.gitlab_type)["labels"]
        ] if pruned else None

        # open items without "meeting" label
        roadmap = [
            d for d in pruned
            if "meeting" not in getattr(d, d.gitlab_type)["labels"]
        ] if pruned else None

        # only past due
        pastdue = [
            d for d in roadmap
            if d.status == "past due"
        ] if roadmap else None

        # only ongoing
        ongoing = [
            d for d in roadmap
            if d.status == "ongoing"
        ] if roadmap else None

        # only upcoming
        upcoming = [
            d for d in roadmap
            if d.status == "upcoming"
        ] if roadmap else None

        return (meetings, pastdue, ongoing, upcoming)

    def extract_tasks(self) -> tuple:
        """
        Extract items relating to blocked/blocking tasks.

        Returns:
            A tuple of (0, 1) where 0 is a list of items which are blocking other items; 1 is a list of items which are blocked by other items.
        """

        # items blocking others
        blocking = [
            d for d in self.issues.items
            if d.status != "complete" and d.blocks
        ]

        # items blocked by others
        blocked = [
            d for d in self.issues.items
            if d.status != "complete" and d.is_blocked_by
        ]

        return (blocking, blocked)

    def generate_metrics(self) -> tuple:
        """
        Generate itemset metrics.

        Returns:
            A tuple of (0, 1, 2) where 0 is a list of items in order of most to least blocking all other items; 1 is a list of items in order of most to least at risk relative to all other items; 2 is a list of items in order of most to least on track relative to all other items.
        """

        # determine blocking issues
        tb = self.determine_top_blockers(self.issues.graph) if self.issues.graph else list()

        # consider determination accurate if not just the same list of all issues
        top_blockers = tb if len(tb) != len(self.issues.items) else list()

        # determine at risk/on track scores for items
        tar = self.determine_most_at_risk(self.epics.items)
        tot = self.determine_most_on_track(self.epics.items)

        # with lack of data lists can both contain same ids
        # isolate ids not shared between lists
        shared_ids = [d.id for d in tar if d.id in [x.id for x in tot]]

        # filter out shared ids at risk
        top_at_risk = [
            d for d in tar
            if d.id not in shared_ids
        ] if len(tar) != len(self.epics.items) else list()

        # filter out shared ids on track
        top_on_track = [
            d for d in tot
            if d.id not in shared_ids
        ] if len(tot) != len(self.epics.items) else list()

        return (top_blockers, top_at_risk, top_on_track)

    def tabulate_nest(self, itemset) -> dict:
        """
        Generate tabular-formatted data under level 1 nodes.

        Args:
            itemset (enum): gitricsGroupEpics || gitricsGroupIssues

        Returns:
            A dictionary where keys represent leve-1 nodes in the graph and corresponding values are lists of dictionaries of each descendant in order of nest.
        """

        result = None

        # check for graph in object
        if itemset.graph:

            # get subitems
            subitems = itemset.items

            result = [
                [x for x in subitems if x.id == d[1]][0]
                for d in list(itemset.graph.edges()) if d[0] == 0
            ]

            # add descendants
            for item in result:
                item.descendants = [
                    [x for x in subitems if x.id == d][0] for d in
                    list(nx.descendants(itemset.graph, item.id))
                ]

        return result
