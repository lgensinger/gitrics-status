import datetime
import os

from glapi import configuration as configuration_glapi

# ANNOTATIONS
ANNOTATION_MISSING = os.environ["ANNOTATION_MISSING"] if "ANNOTATION_MISSING" in os.environ else None
ANNOTATION_RESTRICTED = os.environ["ANNOTATION_RESTRICTED"] if "ANNOTATION_RESTRICTED" in os.environ else None

# ICONS
ICON_COMPLETE = "&#x2714;"
ICON_ONGOING = "&#x27A0;"
ICON_PASTDUE = "&#x2757;"
ICON_UPCOMING = "&#x2738;"

# RENDER
RENDER_FORMATS = [d.strip().lower() for d in os.environ["RENDER_FORMATS"].split(",")] if "RENDER_FORMATS" in os.environ else ["md"]

# STYLE
EMAIL_ACCENT_COLOR = "#00a0c4"
EMAIL_ACCENT_COLOR_LIGHT = "#3aa5bd"
EMAIL_LINE_HEIGHT = "130%"
EMAIL_TABLE_FILL_COLOR = "#f0f0f0"

# TIMING
today_iso = (datetime.datetime.now()).strftime(configuration_glapi.DATE_ISO_8601)
DATE_MEETING = os.environ["DATE_MEETING"] if "DATE_MEETING" in os.environ else today_iso
DATE_PUBLISH = os.environ["DATE_PUBLISH"] if "DATE_PUBLISH" in os.environ else today_iso
DATE_PUBLISH_FUTURE = os.environ["DATE_PUBLISH_FUTURE"] if "DATE_PUBLISH_FUTURE" in os.environ else (datetime.datetime.now() + datetime.timedelta(days=30)).strftime(configuration_glapi.DATE_ISO_8601)
DATE_PUBLISH_PAST = os.environ["DATE_PUBLISH_PAST"] if "DATE_PUBLISH_PAST" in os.environ else (datetime.datetime.now() - datetime.timedelta(days=7)).strftime(configuration_glapi.DATE_ISO_8601)
