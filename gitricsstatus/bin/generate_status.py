#!/usr/bin/env python3

import argparse
import os

import networkx as nx

from gitrics.bin.parameters import param_api, param_group, param_token
from gitrics.group.group import gitricsGroup
from gitrics.utilities import ci_lower_bound

import gitricsstatus

from gitricsstatus import configuration
from gitricsstatus.bin.parameters import param_formats, param_meeting, param_missing, param_output, param_prune_by_ownership, param_publish, param_publish_future, param_publish_past, param_recipients, param_restricted, param_sender, param_title, param_write_to_file
from gitricsstatus.userstatus.document import UserStatus
from gitricsstatus.partials import ACTIVITY, DATA_INPUT, METRICS, ROADMAP, TASKS

from glapi import configuration as glapi_config
from glapi.group import GitlabGroup

def main(group_id: str = None, epics: list = None, issues: list = None, formats: list = None, title: str = None, prune_by_ownership: bool = None, users: list = None, date_publish_future: str = None, date_publish_past: str = None, date_meeting:str = None, date_publish: str = None, pattern_missing: str = None, pattern_restricted: str = None, directory_path_output: str = None, token: str = None, version: str = None, write_to_file: bool = False, email_host: str = None, sender: str = None, recipients: list = None, return_data: bool = False) -> dict:
    """
    Generate list of blockers in directional network of blocking issues.

    Args:
        date_meeting (string): iso 8601 date value
        date_publish (string): iso 8601 date value
        date_publish_future (string): iso 8601 date value
        date_publish_past (string): iso 8601 date value
        directory_path_output (string): directory path for output
        email_host (string): email domain from which to send
        epics (list): dictionaries where each is a GitLab Epic
        formats (list): email, md, pdf types to render output for
        group_id (string): GitLab Group id
        issues (list): dictionaries where each is a GitLab Issue
        pattern_missing (string): RegEx pattern representing a missing annotation
        pattern_restricted (string): RegEx pattern representing a restricted annotation
        prune_by_ownership (boolean): TRUE if data should be pruned to only items with ownership attribution
        recipients (list) email addresses to send to
        return_data (boolean): TRUE if data should be returned
        sender (string): email address to send from
        title (string): title of report
        token (string): GitLab personal access or deploy token
        users (list): dictionaries where each is a GitLab User
        version (string): GitLab api version represented as a base url
        write_to_file (boolean): TRUE if results should render to filesystem

    Returns:
        A dictionary of key/value pairs to represent a document of set of documents with rendered content and associated metadata.
    """

    result = None

    # parse arguments
    opt = parse_args()

    # determine params
    ANNOTATION_MISSING = pattern_missing if pattern_missing else opt.missing
    ANNOTATION_RESTRICTED = pattern_restricted if pattern_restricted else opt.restricted

    DATE_MEETING = date_meeting if date_meeting else opt.meeting
    DATE_PUBLISH = date_publish if date_publish else opt.publish
    DATE_PUBLISH_FUTURE = date_publish_future if date_publish_future else opt.publish_future
    DATE_PUBLISH_PAST = date_publish_past if date_publish_past else opt.publish_past

    DIRECTORY_PATH_OUTPUT = directory_path_output if directory_path_output else opt.output

    EMAIL_HOST = os.environ["EMAIL_HOST"] if "EMAIL_HOST" in os.environ else email_host
    EMAIL_RECIPIENTS = recipients if recipients else opt.recipients
    EMAIL_SENDER = sender if sender else opt.sender

    GROUP_ID = group_id if group_id else opt.group

    PRUNE_BY_OWNERSHIP = prune_by_ownership if prune_by_ownership else opt.prune_by_ownership

    RENDER_FORMATS = [d.strip().lower() for d in formats] if formats else [d.strip().lower() for d in opt.formats.split(",")]
    print(RENDER_FORMATS)
    TITLE = title if title else opt.title

    TOKEN = token if token else opt.token
    VERSION = version if version else opt.api

    WRITE_TO_FILE = write_to_file if write_to_file else opt.write_to_file

    # initialize
    g = GitlabGroup(id=GROUP_ID, token=TOKEN, version=VERSION)

    # get raw data
    epics = epics if epics else g.extract_epics(get_notes=True)
    issues = issues if issues else g.extract_issues(get_notes=True, get_links=True)
    users = users if users else g.extract_users()
    print("epics", len(epics) if epics else None, "issues", len(issues) if issues else None, "users", len(users))
    # initialize gitics
    gg = gitricsGroup(group=g.group, users=users)

    # determine template directory path
    directory_path_gsd = gitricsstatus.__file__.split("/__init__.py")[0]
    directory_path_status = "/".join([d for d in UserStatus.__module__.split(".")[1:-1] if d != ""])
    directory_path_templates = os.path.join(directory_path_gsd, directory_path_status, "templates", "md")

    # initialize document renderer
    gsd = UserStatus(
        directory_path_output=DIRECTORY_PATH_OUTPUT,
        directory_path_templates=directory_path_templates,
        email_host=EMAIL_HOST,
        title=TITLE
    )

    # loop through items
    for item in epics + issues:

        # assign ownership
        item.ownership = gg.determine_ownership(item)

    # check if data should be pruned by user
    if PRUNE_BY_OWNERSHIP:

        # update result
        result = dict()

        # determine users to generate status for
        for user in users:

            # prune by ownership
            pruned = gg.prune(epics + issues, { "ownership": user.id })

            # only proceed with owners
            if len(pruned) > 0:
                print(user.id)
                # generate document
                CONTENT = gsd.create(pruned, gg, DATE_PUBLISH_FUTURE, DATE_PUBLISH_PAST, DATE_MEETING, DATE_PUBLISH, ANNOTATION_MISSING, ANNOTATION_RESTRICTED)

                # update result
                result[user.id] = dict()

                # expose rendered markdown (since it is needed no matter the output)
                if "md" in RENDER_FORMATS:
                    result[user.id]["md"] = CONTENT

                # render email
                if "email" in RENDER_FORMATS:
                    result[user.id]["email"] = gsd.format_email(CONTENT)
                    result[user.id]["send_to"] = user.user["public_email"] if "public_email" in user.user else None

                # file path
                if WRITE_TO_FILE:
                    result[user.id]["file_path"] = os.path.join(gsd.directory_path_output, "%s.md" % user.id)

    else:
        print("USE ALL")
        # update results
        result = { "all": dict() }

        # generate document
        CONTENT = gsd.create(epics + issues, gg, DATE_PUBLISH_FUTURE, DATE_PUBLISH_PAST, DATE_MEETING, DATE_PUBLISH, ANNOTATION_MISSING, ANNOTATION_RESTRICTED)

        # update result

        # expose rendered markdown (since it is needed no matter the output)
        if "md" in RENDER_FORMATS:
            result["all"]["md"] = CONTENT

        # render email
        if "email" in RENDER_FORMATS:
            result["all"]["email"] = gsd.format_email(CONTENT)
            result["all"]["send_to"] = recipients

        # file path
        if WRITE_TO_FILE:
            result["all"]["file_path"] = os.path.join(gsd.directory_path_output, "all.md")

    # loop through result keys which each reprsent a discrete report
    for k in result:

        # get report values
        report = result[k]

        # write file(s)
        if WRITE_TO_FILE:
            gsd.render(
                content=report["md"],
                file_path=report["file_path"]
            )

        # email document(s)
        if "email" in RENDER_FORMATS and EMAIL_HOST and "EMAIL_PASSWORD" in os.environ and EMAIL_SENDER and EMAIL_RECIPIENTS:
            print("TO", EMAIL_RECIPIENTS, "FROM", EMAIL_SENDER)
            gsd.email(
                html=report["email"],
                recipients=EMAIL_RECIPIENTS,
                sender=EMAIL_SENDER,
                text=report["md"]
            )

    if return_data: return result

def parse_args():
    """
    Parse user input.

    Returns:
        A Namespace with attributes for each provided argument.
    """

    # create arguments
    parser = argparse.ArgumentParser()

    # define optional values
    param_api(parser)
    param_formats(parser)
    param_group(parser)
    param_meeting(parser)
    param_missing(parser)
    param_output(parser)
    param_prune_by_ownership(parser)
    param_publish(parser)
    param_publish_future(parser)
    param_publish_past(parser)
    param_recipients(parser)
    param_restricted(parser)
    param_sender(parser)
    param_title(parser)
    param_token(parser)
    param_write_to_file(parser)

    # parse user input
    args, unknown = parser.parse_known_args()

    return args

if __name__ == "__main__":
    main()
