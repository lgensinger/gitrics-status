from gitricsstatus import configuration as configuration

from mkdpdf import configuration as configuration_mkdpdf

def param_formats(parser):
    """
    Add formats parameter.
    """

    parser.add_argument("--formats",
        default=configuration.RENDER_FORMATS,
        help="Comma-delimitered list of output formats to render from available, email, md, pdf."
    )

def param_meeting(parser):
    """
    Add meeting parameter.
    """

    parser.add_argument("--meeting",
        type=str,
        default=configuration.DATE_MEETING,
        help="Meeting date for which status document is prep for."
    )

def param_missing(parser):
    """
    Add missing parameter.
    """

    parser.add_argument("--missing",
        type=str,
        default=configuration.ANNOTATION_MISSING,
        help="RegEx pattern representing a missing annotation."
    )

def param_output(parser):
    """
    Add output parameter.
    """

    parser.add_argument("--output",
        type=str,
        default=configuration_mkdpdf.DIRECTORY_PATH_OUTPUT,
        help="Directory path for output of artifacts."
    )

def param_prune_by_ownership(parser):
    """
    Add prune-by-ownership parameter.
    """

    parser.add_argument("--prune-by-ownership",
        action="store_true",
        default=False,
        help="Prune results by items with ownership."
    )

def param_publish(parser):
    """
    Add publish parameter.
    """

    parser.add_argument("--publish",
        type=str,
        default=configuration.DATE_PUBLISH,
        help="Publish date of status document."
    )

def param_publish_future(parser):
    """
    Add publish-future parameter.
    """

    parser.add_argument("--publish-future",
        type=str,
        default=configuration.DATE_PUBLISH_FUTURE,
        help="Future publish date for document."
    )

def param_publish_past(parser):
    """
    Add publish-past parameter.
    """

    parser.add_argument("--publish-past",
        type=str,
        default=configuration.DATE_PUBLISH_PAST,
        help="Past publish date for document."
    )

def param_recipients(parser):
    """
    Add recipients parameter.
    """

    parser.add_argument("--recipients",
        type=str,
        help="Comma-delimitered list of email addresses to send to."
    )

def param_restricted(parser):
    """
    Add restricted parameter.
    """

    parser.add_argument("--restricted",
        type=str,
        default=configuration.ANNOTATION_RESTRICTED,
        help="RegEx pattern representing a restricted annotation."
    )

def param_sender(parser):
    """
    Add sender parameter.
    """

    parser.add_argument("--sender",
        type=str,
        help="Email address to send from."
    )

def param_title(parser):
    """
    Add title parameter.
    """

    parser.add_argument("--title",
        type=str,
        default=None,
        help="Title of report."
    )

def param_write_to_file(parser):
    """
    Add write-to-file parameter.
    """

    parser.add_argument("--write-to-file",
        action="store_true",
        default=False,
        help="Write generated document(s) to filesystem."
    )
